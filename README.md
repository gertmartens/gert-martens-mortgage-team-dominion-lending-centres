As a Grande Prairie mortgage professional, Gert is determined to exceed your expectations when it comes to getting your mortgage. With the backing of Dominion Lending Centres, she can offer you the best rate, term and options for your mortgage. Give her a call today at (780) 933-0109 to get started!

Address: 10002 100 Ave, Unit 102, Grande Prairie, AB T8V 0V2, CAN

Phone: 780-933-0109
